/**
 * File main.rs
 *
 * Riccardo Sacchetto
 * License GNU GPL v3.0 or later
*/

use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};
use webkit2gtk::{WebViewExt};

fn main() {
    // Create a new GTK application
    let app = Application::builder()
        .application_id("it.nexxontech.whatstux")
        .build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run();
}

fn build_ui(app: &Application) {
    // Create a window and set the title
    let window = ApplicationWindow::builder()
        .application(app)
        .title("WhatsTux - WhatsApp Web Client")
        .build();

    // Create a new WebView
    let browser = webkit2gtk::WebView::new();

    // Load WhatsApp Web page
    browser.load_uri("https://web.whatsapp.com");

    // Add WebView to the GTK window
    window.set_child(Some(&browser));

    // Show all widgets
    window.show_all();

    // Present window to the user
    window.present();
}
